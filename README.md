## Quick start

1. Download the latest stable
2. Run `yarn run setup` to install dependencies
3. Run `yarn run start` and go to `http://localhost:8000`

## License

The code is available under the [MIT license](LICENSE.md).
